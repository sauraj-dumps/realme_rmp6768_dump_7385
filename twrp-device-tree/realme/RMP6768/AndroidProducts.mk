#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_RMP6768.mk

COMMON_LUNCH_CHOICES := \
    omni_RMP6768-user \
    omni_RMP6768-userdebug \
    omni_RMP6768-eng
